package bsu.sc.four.lab.ui.server;

import bsu.sc.four.lab.util.SocketServer;

import javax.swing.UIManager;
import javax.swing.UnsupportedLookAndFeelException;

public class ServerFrame extends javax.swing.JFrame {

    public SocketServer server;
<<<<<<< HEAD
    public String invalidWords = "D:\\Programming\\Labs\\labs\\words.txt";
    public String filePath = "D:\\Programming\\Labs\\labs\\Data.xml";
//    public String invalidWords = "/home/vlad/Documents/Univercity/labs/Words.txt";
//    public String filePath = "/home/vlad/Documents/Univercity/labs/Data.xml";
//    public String invalidWords = "/home/vabramov/Documents/Univercity/labs/Words.txt";
//    public String filePath = "/home/vabramov/Documents/Univercity/labs/Data.xml";
=======
//    public String invalidWords = "D:\\Programming\\Labs\\labs\\words.txt";
//    public String filePath = "D:\\Programming\\Labs\\labs\\Data.xml";
//    public String invalidWords = "/home/vlad/Documents/Univercity/labs/Words.txt";
//    public String filePath = "/home/vlad/Documents/Univercity/labs/Data.xml";
    public String invalidWords = "/home/vabramov/Documents/Univercity/labs/Words.txt";
    public String filePath = "/home/vabramov/Documents/Univercity/labs/Data.xml";
>>>>>>> 467e6a535c00d72d5771a7d0a6c5434a045bfc30

    public ServerFrame() {
        initComponents();

        jTextArea1.setEditable(false);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        startServerB = new javax.swing.JButton();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTextArea1 = new javax.swing.JTextArea();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Chat server");

        startServerB.setText("Start Server");
        startServerB.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                startServerBActionPerformed(evt);
            }
        });

        jTextArea1.setColumns(20);
        jTextArea1.setFont(new java.awt.Font("Consolas", 0, 12)); // NOI18N
        jTextArea1.setRows(5);
        jScrollPane1.setViewportView(jTextArea1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 608, Short.MAX_VALUE)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(startServerB)
                        .addGap(0, 0, 0)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(startServerB)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 287, Short.MAX_VALUE)
                .addContainerGap())
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void startServerBActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_startServerBActionPerformed
        server = new SocketServer(this);
        startServerB.setEnabled(false);
    }//GEN-LAST:event_startServerBActionPerformed

    public void RetryStart(int port){
        if(server != null) {
            server.stop();
        }
        server = new SocketServer(this, port);
    }

    public static void main(String args[]) {

        try{
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        }
        catch(ClassNotFoundException | IllegalAccessException | InstantiationException | UnsupportedLookAndFeelException ex){
            System.out.println("Look & Feel Exception");
        }

        java.awt.EventQueue.invokeLater(new Runnable() {
            @Override
            public void run() {
                new ServerFrame().setVisible(true);
            }
        });
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JScrollPane jScrollPane1;
    public javax.swing.JTextArea jTextArea1;
    private javax.swing.JButton startServerB;
    // End of variables declaration//GEN-END:variables
}

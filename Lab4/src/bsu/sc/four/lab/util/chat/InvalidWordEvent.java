package bsu.sc.four.lab.util.chat;

import javax.swing.*;
import java.awt.*;
import java.util.List;

/**
 * @author vabramov
 */
public class InvalidWordEvent extends AWTEvent {

    public static final int EVENT_ID = AWTEvent.RESERVED_ID_MAX + 999;
    private static final long serialVersionUID = -6263059597224199310L;
    private static String replaces = "###############################################################################";
    private List<String> invalidWords;
    private String validText;

    public InvalidWordEvent(JTextArea component, List<String> invalidWords, String text) {
        super(component, EVENT_ID);
        this.invalidWords = invalidWords;

        for (String word : invalidWords) {
            text = text.replaceAll(word, replaces.substring(0, word.length()));
        }
        validText = text;
    }

    public String getValidText() {
        return validText;
    }
<<<<<<< HEAD

    public List<String> getInvalidWords() {
        return invalidWords;
    }
=======
>>>>>>> 467e6a535c00d72d5771a7d0a6c5434a045bfc30
}

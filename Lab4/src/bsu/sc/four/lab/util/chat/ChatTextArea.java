package bsu.sc.four.lab.util.chat;

import javax.swing.*;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.text.BadLocationException;
import java.awt.*;
import java.util.List;

/**
 * @author vabramov
 */
public class ChatTextArea extends JTextArea implements DocumentListener {
    private static final long serialVersionUID = -3452863336045250025L;
    private static EventQueue eventQueue;
    private ChatListener chatListener;
    private InvalidWords iw = new InvalidWords();

    public ChatTextArea() {
        this.getDocument().addDocumentListener(this);
        eventQueue = Toolkit.getDefaultToolkit().getSystemEventQueue();
    }

    public void addChatListener(ChatListener chatListener) {
        this.chatListener = chatListener;
    }

    public void setInvalidWords(InvalidWords iw) {
        this.iw = iw;
    }

    @Override
    protected void processEvent(AWTEvent e) {
        if (e instanceof InvalidWordEvent) {
            if (chatListener != null)
                chatListener.invalidString((InvalidWordEvent) e);
        } else
            super.processEvent(e);
    }

    @Override
    public void insertUpdate(DocumentEvent e) {
        changedUpdate(e);
    }

    @Override
    public void removeUpdate(DocumentEvent e) {
        changedUpdate(e);
    }

    @Override
    public void changedUpdate(DocumentEvent e) {
        try {
            String text = e.getDocument().getText(0, e.getDocument().getLength());
            List<String> words = iw.isInvalid(text);
            if (words.size() != 0) {
                eventQueue.postEvent(new InvalidWordEvent(this, words, text));
            }
        } catch (BadLocationException ex) {
            System.out.println(ex);
        }
    }

}

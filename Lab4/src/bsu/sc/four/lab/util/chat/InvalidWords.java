package bsu.sc.four.lab.util.chat;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;

/**
 * @author vlad
 */
public class InvalidWords {
    private Set<String> words;

    public InvalidWords() {
        words = new HashSet<>();
    }

    public InvalidWords(String file) throws IOException {
        try (BufferedReader br = new BufferedReader(new FileReader(file))) {
            String line;
            words = new HashSet<>();
            while ((line = br.readLine()) != null) {
                StringTokenizer st = new StringTokenizer(line, ";");
                while (st.hasMoreTokens()) {
                    words.add(st.nextToken());
                }
            }
        }
    }

    public static InvalidWords parse(String str) {
        InvalidWords iw = new InvalidWords();
        iw.words = new HashSet<>();
        StringTokenizer st = new StringTokenizer(str, ";");
        while (st.hasMoreTokens()) {
            iw.words.add(st.nextToken());
        }
        return iw;
    }

    public List<String> isInvalid(String str) {
        ArrayList<String> invalidWords = new ArrayList<>();
        for (String word : words) {
            if (str.contains(word)) {
                invalidWords.add(word);
            }
        }
        return invalidWords;
    }

    public String toString() {
        StringBuffer sb = new StringBuffer();
        for (String word : words) {
            sb.append(word).append(";");
        }
        return sb.toString();
    }
}

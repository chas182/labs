package bsu.sc.four.lab.util.chat;

import java.util.EventListener;

/**
 * @author vabramov
 */
public interface ChatListener extends EventListener {
    void invalidString(InvalidWordEvent e);
}

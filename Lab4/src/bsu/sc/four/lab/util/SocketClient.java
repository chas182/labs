package bsu.sc.four.lab.util;

import bsu.sc.four.lab.ui.client.ChatFrame;
import bsu.sc.four.lab.util.chat.InvalidWords;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.InetAddress;
import java.net.Socket;

/**
 * @author vabramov
 */
public class SocketClient implements Runnable {

    public int port;
    public String serverAddress;
    public Socket socket;
    public ChatFrame ui;
    public ObjectInputStream In;
    public ObjectOutputStream Out;

    public SocketClient(ChatFrame frame) throws IOException {
        ui = frame;
        this.serverAddress = ui.serverAddress;
        this.port = ui.port;
        socket = new Socket(InetAddress.getByName(serverAddress), port);

        Out = new ObjectOutputStream(socket.getOutputStream());
        Out.flush();
        In = new ObjectInputStream(socket.getInputStream());
    }

    @Override
    public void run() {
        boolean keepRunning = true;
        while (keepRunning) {
            try {
                Message msg = (Message) In.readObject();
                System.out.println("Incoming : " + msg.toString());

                switch (msg.type) {
                    case "message":
                        if (msg.recipient.equals(ui.username)) {
                            ui.chatTextArea.append("[" + msg.sender + " > Me] : " + msg.content + "\n");
                        } else {
                            ui.chatTextArea.append("[" + msg.sender + " > " + msg.recipient + "] : " + msg.content + "\n");
                        }
                        break;
                    case "login":
                        if (msg.content.equals("TRUE")) {
                            ui.loginB.setEnabled(false);
                            ui.signUpB.setEnabled(false);
                            ui.sendMessageB.setEnabled(true);
                            ui.chatTextArea.append("[SERVER > Me] : Login Successful\n");
                            ui.loginTF.setEnabled(false);
                            ui.passwordTF.setEnabled(false);
                        } else {
                            ui.chatTextArea.append("[SERVER > Me] : Login Failed\n");
                        }
                        break;
                    case "invalidWords":
                        ui.chatTextArea.setInvalidWords(InvalidWords.parse(msg.content));
                        break;
                    case "test":
                        ui.connectB.setEnabled(false);
                        ui.loginB.setEnabled(true);
                        ui.signUpB.setEnabled(true);
                        ui.loginTF.setEnabled(true);
                        ui.passwordTF.setEnabled(true);
                        ui.hostTF.setEditable(false);
                        ui.portTF.setEditable(false);
                        break;
                    case "newuser":
                        if (!msg.content.equals(ui.username)) {
                            boolean exists = false;
                            for (int i = 0; i < ui.model.getSize(); i++) {
                                if (ui.model.getElementAt(i).equals(msg.content)) {
                                    exists = true;
                                    break;
                                }
                            }
                            if (!exists) {
                                ui.model.addElement(msg.content);
                            }
                        }
                        break;
                    case "signup":
                        if (msg.content.equals("TRUE")) {
                            ui.loginB.setEnabled(false);
                            ui.signUpB.setEnabled(false);
                            ui.sendMessageB.setEnabled(true);
                            ui.chatTextArea.append("[SERVER > Me] : Singup Successful\n");
                        } else {
                            ui.chatTextArea.append("[SERVER > Me] : Signup Failed\n");
                        }
                        break;
                    case "signout":
                        if (msg.content.equals(ui.username)) {
                            ui.chatTextArea.append("[" + msg.sender + " > Me] : Bye\n");
                            ui.connectB.setEnabled(true);
                            ui.sendMessageB.setEnabled(false);
                            ui.hostTF.setEditable(true);
                            ui.portTF.setEditable(true);

                            for (int i = 1; i < ui.model.size(); i++) {
                                ui.model.removeElementAt(i);
                            }

                            ui.clientThread.stop();
                        } else {
                            ui.model.removeElement(msg.content);
                            ui.chatTextArea.append("[" + msg.sender + " > All] : " + msg.content + " has signed out\n");
                        }
                        break;
                    default:
                        ui.chatTextArea.append("[SERVER > Me] : Unknown message type\n");
                        break;
                }
            } catch (Exception ex) {
                keepRunning = false;
                ui.chatTextArea.append("[Application > Me] : Connection Failure\n");
                ui.connectB.setEnabled(true);
                ui.hostTF.setEditable(true);
                ui.portTF.setEditable(true);

                for (int i = 1; i < ui.model.size(); i++) {
                    ui.model.removeElementAt(i);
                }

                ui.clientThread.stop();

                System.out.println("Exception SocketClient run()");
                ex.printStackTrace();
            }
        }
    }

    public void send(Message msg) {
        try {
            Out.writeObject(msg);
            Out.flush();
            System.out.println("Outgoing : " + msg.toString());
        } catch (IOException ex) {
            System.out.println("Exception SocketClient send()");
        }
    }
}

package bsu.sc.first.exam;

import java.awt.Color;
import java.awt.Graphics;
import static java.lang.Thread.sleep;
import java.util.Random;
import javax.swing.JPanel;

/**
 *
 * @author Vlad
 */
public class MainJFrame extends javax.swing.JFrame {

    private int a = 70, b = 50, k = 5;
    private Color renderedColor = Color.white;
    private Random r = new Random();

    public MainJFrame() {
        initComponents();
        new ColorThread().start();
        new SizeThread(this).start();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel1 = new JPanel() {
            @Override
            public void paint(Graphics g) {
                taskPaint(g);
            }
        };

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);

        jPanel1.setBackground(new java.awt.Color(255, 255, 255));

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 400, Short.MAX_VALUE)
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 300, Short.MAX_VALUE)
        );

        getContentPane().add(jPanel1, java.awt.BorderLayout.CENTER);

        pack();
    }// </editor-fold>//GEN-END:initComponents

    public void taskPaint(Graphics g) {
        g.setColor(renderedColor);
        g.fillRect(20, 20, a + 20, b + 20);
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) {
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Windows".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException | javax.swing.UnsupportedLookAndFeelException ex) {
            System.err.println(ex.getMessage());
        }
        //</editor-fold>

        new MainJFrame().setVisible(true);
    }

    class ColorThread extends Thread {

        @Override
        public void run() {
            while (true) {
                synchronized (renderedColor) {
                    renderedColor = Color.getHSBColor(StrictMath.abs(r.nextInt(250)), StrictMath.abs(r.nextInt(250)), StrictMath.abs(r.nextInt(250)));
                }
                jPanel1.repaint();
                try {
                    sleep(2500);
                } catch (InterruptedException ex) {
                    System.err.println(ex.getMessage());
                }
            }
        }
    }

    class SizeThread extends Thread {

        private Object sync;

        public SizeThread(Object sync) {
            this.sync = sync;
        }

        @Override
        public void run() {
            while (true) {
                synchronized (sync) {
                    a += k;
                    b += k;
                    k += 5;
                }
                jPanel1.repaint();
                try {
                    sleep(1500);
                } catch (InterruptedException ex) {
                    System.err.println(ex.getMessage());
                }
            }
        }
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPanel jPanel1;
    // End of variables declaration//GEN-END:variables
}

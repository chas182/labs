package bsu.sc.second.lab.windows;

import bsu.sc.second.lab.util.RuleFileFilter;
import bsu.sc.second.lab.util.Translator;
import java.io.File;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;

/**
 *
 * @author Vlad
 * @since 1.1
 */
public class RulesDialog extends javax.swing.JDialog {

    public RulesDialog(java.awt.Frame parent, boolean modal) {
        super(parent, modal);
        initComponents();
        fileChooser.setFileFilter(new RuleFileFilter());
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        editRulesPanel = new javax.swing.JPanel();
        jScrollPane2 = new javax.swing.JScrollPane();
        editRulesArea = new javax.swing.JTextArea();
        editRulesArea.getDocument().addDocumentListener(new javax.swing.event.DocumentListener() {
            @Override
            public void insertUpdate(javax.swing.event.DocumentEvent e) {
                modified = true;
            }

            @Override
            public void removeUpdate(javax.swing.event.DocumentEvent e) {
                modified = true;
            }

            @Override
            public void changedUpdate(javax.swing.event.DocumentEvent e) {
                modified = true;
            }
        });
        jMenuBar1 = new javax.swing.JMenuBar();
        jMenu1 = new javax.swing.JMenu();
        saveButton = new javax.swing.JMenuItem();
        saveAsButton = new javax.swing.JMenuItem();
        jSeparator1 = new javax.swing.JPopupMenu.Separator();
        exitButton = new javax.swing.JMenuItem();

        setDefaultCloseOperation(javax.swing.WindowConstants.DO_NOTHING_ON_CLOSE);
        setResizable(false);
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosing(java.awt.event.WindowEvent evt) {
                formWindowClosing(evt);
            }
        });

        editRulesPanel.setLayout(new java.awt.BorderLayout());

        editRulesArea.setColumns(20);
        editRulesArea.setRows(5);
        jScrollPane2.setViewportView(editRulesArea);

        editRulesPanel.add(jScrollPane2, java.awt.BorderLayout.CENTER);

        jMenu1.setText("Файл");

        saveButton.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_S, java.awt.event.InputEvent.CTRL_MASK));
        saveButton.setText("Сохранить");
        saveButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                saveButtonActionPerformed(evt);
            }
        });
        jMenu1.add(saveButton);

        saveAsButton.setText("Сохранить как ...");
        saveAsButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                saveAsButtonActionPerformed(evt);
            }
        });
        jMenu1.add(saveAsButton);
        jMenu1.add(jSeparator1);

        exitButton.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_Q, java.awt.event.InputEvent.CTRL_MASK));
        exitButton.setText("Выход");
        jMenu1.add(exitButton);

        jMenuBar1.add(jMenu1);

        setJMenuBar(jMenuBar1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(editRulesPanel, javax.swing.GroupLayout.PREFERRED_SIZE, 400, javax.swing.GroupLayout.PREFERRED_SIZE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(editRulesPanel, javax.swing.GroupLayout.PREFERRED_SIZE, 279, javax.swing.GroupLayout.PREFERRED_SIZE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void formWindowClosing(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowClosing
        if (modified) {
            if (save()) {
                dispose();
                return;
            }
        }
        dispose();
    }//GEN-LAST:event_formWindowClosing

    private void saveButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_saveButtonActionPerformed
        saveRules();
    }//GEN-LAST:event_saveButtonActionPerformed

    private void saveAsButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_saveAsButtonActionPerformed
        saveAsRules();
    }//GEN-LAST:event_saveAsButtonActionPerformed

    public void editRules() {
        saveButton.setEnabled(true);
        saveAsButton.setEnabled(true);
        setTitle(Translator.getTranslator().getRulesFile().getName());
        editRulesArea.setText(Translator.getTranslator().stringRepresentationOfRules());
        modified = false;
        setVisible(true);
    }

    /**
     * @return {@code true} if user clicked {@code YES} button, {@code false} in
     * other cases.
     */
    private boolean save() {
        switch (JOptionPane.showConfirmDialog(
                this,
                "Файл был изменен.\nСохранить?",
                "Выход",
                JOptionPane.YES_NO_OPTION)) {
            case JOptionPane.YES_OPTION:
                saveRules();
                return true;
            case JOptionPane.NO_OPTION:
            case JOptionPane.CLOSED_OPTION:
                return false;
        }
        return false;
    }

    private void saveAsRules() {
        fileChooser.setCurrentDirectory(Translator.getTranslator().getRulesFile());
        if (JFileChooser.APPROVE_OPTION == fileChooser.showSaveDialog(this)) {
            File selected = fileChooser.getSelectedFile();

            Translator.loadFromStringAndSaveAs(selected, editRulesArea.getText());

            ((MainFrame) this.getParent()).setNewTitle();
            setTitle(Translator.getTranslator().getRulesFile().getName());

            modified = false;
            ((MainFrame) this.getParent()).refresh();
        }
    }

    private void saveRules() {
        Translator.loadFromStringAndSave(editRulesArea.getText());
        modified = false;
        ((MainFrame) this.getParent()).refresh();
    }
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JTextArea editRulesArea;
    private javax.swing.JPanel editRulesPanel;
    private javax.swing.JMenuItem exitButton;
    private javax.swing.JMenu jMenu1;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JPopupMenu.Separator jSeparator1;
    private javax.swing.JMenuItem saveAsButton;
    private javax.swing.JMenuItem saveButton;
    // End of variables declaration//GEN-END:variables
    boolean modified = false;
    private JFileChooser fileChooser = new JFileChooser();
}

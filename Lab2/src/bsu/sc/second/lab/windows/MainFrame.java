package bsu.sc.second.lab.windows;

import bsu.sc.second.lab.util.LatinTextAreaListener;
import bsu.sc.second.lab.util.RuleFileFilter;
import bsu.sc.second.lab.util.Translator;
import java.awt.event.KeyListener;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;

/**
 *
 * @author Vlad
 * @since 1.0
 */
public class MainFrame extends javax.swing.JFrame {

    public static final String TITLE = "Лабораторная 2";

    public MainFrame() {
        initComponents();
        fileDialog.setFileFilter(new RuleFileFilter());
        setResizable(false);
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        clearButton = new javax.swing.JButton();
        jScrollPane2 = new javax.swing.JScrollPane();
        cirilicTextField = new javax.swing.JTextArea();
        jScrollPane1 = new javax.swing.JScrollPane();
        latinTextField = new javax.swing.JTextArea();
        bsu.sc.second.lab.util.LatinTextAreaListener listener = new bsu.sc.second.lab.util.LatinTextAreaListener(cirilicTextField);
        latinTextField.getDocument().addDocumentListener(listener);
        latinTextField.addKeyListener(listener);
        jMenuBar1 = new javax.swing.JMenuBar();
        fileMenu = new javax.swing.JMenu();
        fileOpenButton = new javax.swing.JMenuItem();
        editButton = new javax.swing.JMenuItem();
        jSeparator1 = new javax.swing.JPopupMenu.Separator();
        exitButton = new javax.swing.JMenuItem();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setTitle("Лабораторная 2");

        clearButton.setText("Очистить все");
        clearButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                clearButtonActionPerformed(evt);
            }
        });

        cirilicTextField.setEditable(false);
        cirilicTextField.setColumns(20);
        cirilicTextField.setRows(5);
        jScrollPane2.setViewportView(cirilicTextField);

        latinTextField.setColumns(20);
        latinTextField.setRows(5);
        latinTextField.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyTyped(java.awt.event.KeyEvent evt) {
                latinTextFieldKeyTyped(evt);
            }
        });
        jScrollPane1.setViewportView(latinTextField);

        fileMenu.setText("Файл");

        fileOpenButton.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_L, java.awt.event.InputEvent.CTRL_MASK));
        fileOpenButton.setText("Загрузить правила");
        fileOpenButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                fileOpen(evt);
            }
        });
        fileMenu.add(fileOpenButton);

        editButton.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_E, java.awt.event.InputEvent.CTRL_MASK));
        editButton.setText("Редиктировать правила");
        editButton.setEnabled(false);
        editButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                editButtonActionPerformed(evt);
            }
        });
        fileMenu.add(editButton);
        fileMenu.add(jSeparator1);

        exitButton.setAccelerator(javax.swing.KeyStroke.getKeyStroke(java.awt.event.KeyEvent.VK_F4, java.awt.event.InputEvent.ALT_MASK));
        exitButton.setText("Выход");
        exitButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                exitButtonActionPerformed(evt);
            }
        });
        fileMenu.add(exitButton);

        jMenuBar1.add(fileMenu);

        setJMenuBar(jMenuBar1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane2, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 430, Short.MAX_VALUE)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(layout.createSequentialGroup()
                        .addComponent(clearButton)
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 141, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jScrollPane2, javax.swing.GroupLayout.PREFERRED_SIZE, 160, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(clearButton)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void exitButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_exitButtonActionPerformed
        System.exit(0);
    }//GEN-LAST:event_exitButtonActionPerformed

    public void setNewTitle() {
        setTitle(TITLE + " - " + Translator.getTranslator().getRulesFile().getName());
    }

    private void fileOpen(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_fileOpen
        if (JFileChooser.APPROVE_OPTION == fileDialog.showOpenDialog(this)) {
            Translator.loadFromFile(fileDialog.getSelectedFile());

            setNewTitle();
            editButton.setEnabled(true);
            refresh();
        }
    }//GEN-LAST:event_fileOpen

    private void editButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_editButtonActionPerformed
        rulesDialog.editRules();
    }//GEN-LAST:event_editButtonActionPerformed

    public void refresh() {
        for (KeyListener listner : latinTextField.getKeyListeners()) {
            if (listner.getClass().equals(LatinTextAreaListener.class)) {
                LatinTextAreaListener lalinListner = (LatinTextAreaListener) listner;
                lalinListner.changedUpdate(latinTextField.getDocument());
            }
        }
    }

    private void clearButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_clearButtonActionPerformed
        cirilicTextField.setText("");
        latinTextField.setText("");
    }//GEN-LAST:event_clearButtonActionPerformed

    private void latinTextFieldKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_latinTextFieldKeyTyped
        if (!Translator.getTranslator().isRuleLoaded()) {
            if (JOptionPane.YES_OPTION == JOptionPane.showConfirmDialog(
                    this,
                    "Правила не заданы.\nХотите их загрузить?",
                    "Ошибка", JOptionPane.YES_NO_OPTION)) {
                fileOpen(null);
            }
        }
    }//GEN-LAST:event_latinTextFieldKeyTyped
    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JTextArea cirilicTextField;
    private javax.swing.JButton clearButton;
    private javax.swing.JMenuItem editButton;
    private javax.swing.JMenuItem exitButton;
    private javax.swing.JMenu fileMenu;
    private javax.swing.JMenuItem fileOpenButton;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JPopupMenu.Separator jSeparator1;
    private javax.swing.JTextArea latinTextField;
    // End of variables declaration//GEN-END:variables
    private JFileChooser fileDialog = new JFileChooser();
    /**
     * Shows the rules and with it we can edit rules.
     *
     * @since 1.1
     */
    private RulesDialog rulesDialog = new RulesDialog(this, true);
}

package bsu.sc.second.lab.util;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.Map;
import java.util.StringTokenizer;
import java.util.TreeMap;
import javax.swing.JOptionPane;

/**
 * {@code Singletone} for translation latin text into cyrillic text.
 *
 * @author Vlad
 * @since 1.0
 */
public final class Translator {

    private static Translator translator = null;
    private Map<String, String> rules = new TreeMap<>();
    private File rulesFile;
    private boolean ruleLoaded = false;

    private Translator() {
    }

    /**
     * Returns instance of this {@code singletone}.
     */
    public static Translator getTranslator() {
        if (translator == null) {
            translator = new Translator();
        }
        return translator;
    }

    /**
     * Returns instance of this {@code singletone} with initialization rules
     * from file.
     *
     * @see #loadFromFile(java.io.File)
     */
    public static Translator getTranslator(File file) {
        if (translator == null) {
            translator = new Translator();
        }
        translator.setRules(file);
        return translator;
    }

    /**
     * Load rules for translate from file with extention {@code .rules}.
     *
     * @param file file that contains rules.
     */
    public static void loadFromFile(File file) {
        if (translator == null) {
            translator = new Translator();
        }
        translator.setRules(file);
    }

    /**
     * Load rules for translate from string representation from
     * {@code JTextArea} in {@code RulesDialog}.
     *
     * @param str {@code String} representation of rules.
     */
    public static void loadFromString(String str) {
        if (translator == null) {
            translator = new Translator();
        }
        translator.setRules(str);
    }

    /**
     * Load rules for translate from string representation from
     * {@code JTextArea} in {@code RulesDialog}.
     *
     * @param str {@code String} representation of rules.
     * @see #loadFromStringAndSaveAs(java.io.File, java.lang.String)
     */
    public static void loadFromStringAndSave(String str) {
        if (translator == null) {
            translator = new Translator();
        }
        translator.writeRulesInFile(translator.rulesFile, str);
    }

    /**
     * Load rules for translate from string representation from
     * {@code JTextArea} in {@code RulesDialog}.
     *
     * @param file file in witch rules will be saved.
     * @param str {@code String} representation of rules.
     */
    public static void loadFromStringAndSaveAs(File file, String str) {
        if (translator == null) {
            translator = new Translator();
        }
        translator.writeRulesInFile(file, str);
    }

    public boolean isRuleLoaded() {
        return ruleLoaded;
    }

    public File getRulesFile() {
        return rulesFile;
    }

    public int getNumberOfRules() {
        return rules.size();
    }

    /**
     * @return a {@code String} of Cyrillic symbol if it exist in the rule,
     * otherwise {@code null}.
     */
    public static String translateLatinToCyrillic(String translate) {
        String cyr = translator.rules.get(translate);
        return (cyr != null) ? cyr : (translator.isLiteral(translate) ? translate : null);
    }

    private boolean isLiteral(String str) {
        if (str.equals(" ") || str.equals("\n") || str.equals("\b")) {
            return true;
        }
        return false;
    }

    public boolean containsLatinSymbol(String symb) {
        return rules.containsKey(symb);
    }

    private void setRules(File f) {
        if (!rules.isEmpty()) {
            rules.clear();
        }
        rulesFile = f;
        parseFile();
    }

    private void setRules(String str) {
        Map<String, String> copy = new TreeMap<>(rules);

        if (!rules.isEmpty()) {
            rules.clear();
        }
        StringTokenizer st = new StringTokenizer(str, "\n");
        while (st.hasMoreTokens()) {
            String token = st.nextToken();
            if (!token.equals("")) {
                String[] parts = token.split("=");
                if (parts.length == 2) {
                    rules.put(parts[0], parts[1]);
                } else {
                    JOptionPane.showMessageDialog(null, "File is invalid!");
                    rules = copy;
                    rulesFile = null;
                }
            }
        }
    }

    private void writeRulesInFile(File file, String rules) {
        setRules(rules);
        try (PrintWriter pw = new PrintWriter(file)) {
            pw.println(toSave());
            this.rulesFile = file;
        } catch (FileNotFoundException fnfe) {
            JOptionPane.showMessageDialog(null, "File not found!");
        }
    }

    private void parseFile() {
        Map<String, String> copy = new TreeMap<>(rules);

        try (BufferedReader br = new BufferedReader(new FileReader(rulesFile))) {
            String str;
            while ((str = br.readLine()) != null) {
                if (!"".equals(str)) {
                    String[] parts = str.split("=");
                    if (parts.length == 2) {
                        rules.put(parts[1], parts[0]);
                    } else {
                        JOptionPane.showMessageDialog(null, "File is invalid!");
                        rules = copy;
                        rulesFile = null;
                    }
                }
            }
            ruleLoaded = true;
        } catch (IOException ioe) {
            System.err.println(ioe.getMessage());
        }
    }

    public String stringRepresentationOfRules() {
        StringBuilder sb = new StringBuilder();
        for (String key : rules.keySet()) {
            sb.append(key);
            sb.append('=');
            sb.append(rules.get(key));
            sb.append('\n');
        }

        return sb.toString();
    }

    private String toSave() {
        StringBuilder sb = new StringBuilder();
        for (String key : rules.keySet()) {
            sb.append(rules.get(key));
            sb.append('=');
            sb.append(key);
            sb.append('\n');
        }

        return sb.toString();
    }
}

package bsu.sc.second.lab.util;

import java.io.File;
import javax.swing.filechooser.FileFilter;

/**
 *
 * @author Vlad
 * @since 1.0
 */
public class RuleFileFilter extends FileFilter {

    @Override
    public boolean accept(File f) {
        if (f.isDirectory()) {
            return true;
        }

        String extension = getExtension(f);
        if (extension != null) {
            return extension.equals("rules");
        }

        return false;
    }

    @Override
    public String getDescription() {
        return "Only rules";
    }

    private String getExtension(File f) {
        String name = f.getName();
        int indexOfDott = name.lastIndexOf('.');

        return (indexOfDott != -1) ? name.substring(indexOfDott + 1) : null;
    }
}

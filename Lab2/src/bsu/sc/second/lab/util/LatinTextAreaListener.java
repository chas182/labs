package bsu.sc.second.lab.util;

import java.awt.event.KeyAdapter;
import javax.swing.JTextArea;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.text.BadLocationException;
import javax.swing.text.Document;

/**
 *
 * @author Vlad
 * @since 1.2
 */
public class LatinTextAreaListener extends KeyAdapter implements DocumentListener {

    private JTextArea cyrillic;

    public LatinTextAreaListener(JTextArea cyrillic) {
        this.cyrillic = cyrillic;
    }

    @Override
    public void insertUpdate(DocumentEvent e) {
        changedUpdate(e);
    }

    @Override
    public void removeUpdate(DocumentEvent e) {
        changedUpdate(e);
    }

    @Override
    public void changedUpdate(DocumentEvent e) {
        try {
            changedUpdate(e.getDocument().getText(0, e.getDocument().getLength()));
        } catch (BadLocationException ex) {
            System.err.println(ex.getMessage());
        }
    }

    public void changedUpdate(Document document) {
        try {
            changedUpdate(document.getText(0, document.getLength()));
        } catch (BadLocationException ex) {
            System.err.println(ex.getMessage());
        }
    }

    private void changedUpdate(String text) {
        StringBuilder sb = new StringBuilder();

        char[] typedArray = text.toCharArray();
        int arrLength = typedArray.length;

        for (int i = 0; i < arrLength; ++i) {
            if (isNonLiteral(typedArray[i])) {
                sb.append(typedArray[i]);
                continue;
            }
            String returned = null, typed;

            if (i + 2 < arrLength) {
                typed = String.valueOf(typedArray[i])
                        + String.valueOf(typedArray[i + 1])
                        + String.valueOf(typedArray[i + 2]);
                returned = Translator.translateLatinToCyrillic(typed);
                if (returned != null) {
                    sb.append(returned);
                    i += 2;
                    continue;
                }
            }

            if (returned == null && i + 1 < arrLength) {
                typed = String.valueOf(typedArray[i])
                        + String.valueOf(typedArray[i + 1]);
                returned = Translator.translateLatinToCyrillic(typed);
                if (returned != null) {
                    sb.append(returned);
                    ++i;
                    continue;
                }
            }

            if (returned == null) {
                typed = typedArray[i] + "";
                returned = Translator.translateLatinToCyrillic(typed);
                if (returned != null) {
                    sb.append(returned);
                } else {
                    sb.append(typedArray[i]);
                }
            }
        }

        cyrillic.setText(sb.toString());
    }

    //<editor-fold defaultstate="collapsed" desc="Some not interesting method :)">
    private boolean isNonLiteral(char keyChar) {
        switch (keyChar) {
            case ' ':
            case '!':
            case '?':
            case '(':
            case ')':
            case '[':
            case ',':
            case '.':
            case '-':
            case '+':
            case '*':
            case '/':
            case '=':
            case ':':
            case ';':
            case '\"':
            case '\'':
            case '_':
            case '^':
            case '&':
            case '%':
            case '$':
            case '#':
            case '@':
            case '>':
            case '<':
                return true;
        }
        return false;
    }
    //</editor-fold>
}

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package bsu.sc.five.lab.ui;

import bsu.sc.five.lab.ui.util.Messages;

import javax.swing.*;
<<<<<<< HEAD
import java.awt.*;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.text.DateFormat;
import java.text.NumberFormat;
import java.util.Currency;
import java.util.Date;
import java.util.Locale;
import java.util.StringTokenizer;
=======
import java.util.Locale;
>>>>>>> 467e6a535c00d72d5771a7d0a6c5434a045bfc30

/**
 *
 * @author vabramov
 */
public class ClientFrame extends javax.swing.JFrame {

<<<<<<< HEAD
    private Date curDate = new Date();

    public ClientFrame() {
        getContentPane().setPreferredSize(new Dimension(400, 200));
        initComponents();
        dateL.setText(DateFormat.getDateInstance(DateFormat.MONTH_FIELD, Messages.RUSSIAN).format(curDate));
        currencyL.setText(NumberFormat.getCurrencyInstance(Messages.RUSSIAN).format(1234567890.99));
        setResizable(false);
=======
    public ClientFrame() {
        initComponents();
>>>>>>> 467e6a535c00d72d5771a7d0a6c5434a045bfc30
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        functionTF = new javax.swing.JTextField();
        functionL = new javax.swing.JLabel();
        resultTextL = new javax.swing.JLabel();
        resultL = new javax.swing.JLabel();
        calculateB = new javax.swing.JButton();
        jToolBar1 = new javax.swing.JToolBar();
        langL = new javax.swing.JLabel();
        jSeparator1 = new javax.swing.JToolBar.Separator();
        dateL = new javax.swing.JLabel();
<<<<<<< HEAD
        jSeparator2 = new javax.swing.JToolBar.Separator();
        currencyL = new javax.swing.JLabel();
=======
>>>>>>> 467e6a535c00d72d5771a7d0a6c5434a045bfc30
        jMenuBar1 = new javax.swing.JMenuBar();
        jMenu1 = new javax.swing.JMenu();
        enUSMenuItem = new javax.swing.JCheckBoxMenuItem();
        ruRuMenuItem = new javax.swing.JCheckBoxMenuItem();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
<<<<<<< HEAD
        setTitle("Lab 5");

        functionL.setText("Function :");

        resultTextL.setText("Result :");

        calculateB.setText("Calculate");
=======
        setTitle(Messages.getMessage("title"));

        functionL.setText(Messages.getMessage("label.function") + " :");

        resultTextL.setText(Messages.getMessage("label.result") + " :");

        calculateB.setText(Messages.getMessage("label.calculate"));
>>>>>>> 467e6a535c00d72d5771a7d0a6c5434a045bfc30
        calculateB.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                calculateBActionPerformed(evt);
            }
        });

<<<<<<< HEAD
        jToolBar1.setFloatable(false);
=======
>>>>>>> 467e6a535c00d72d5771a7d0a6c5434a045bfc30
        jToolBar1.setRollover(true);

        langL.setMaximumSize(new java.awt.Dimension(51, 18));
        langL.setMinimumSize(new java.awt.Dimension(51, 18));
        langL.setOpaque(true);
        langL.setPreferredSize(new java.awt.Dimension(51, 18));
        jToolBar1.add(langL);
        jToolBar1.add(jSeparator1);

<<<<<<< HEAD
        dateL.setMaximumSize(new java.awt.Dimension(80, 18));
        dateL.setMinimumSize(new java.awt.Dimension(80, 18));
        dateL.setPreferredSize(new java.awt.Dimension(80, 18));
        jToolBar1.add(dateL);
        jToolBar1.add(jSeparator2);

        currencyL.setMaximumSize(new java.awt.Dimension(90, 18));
        currencyL.setMinimumSize(new java.awt.Dimension(90, 18));
        currencyL.setPreferredSize(new java.awt.Dimension(90, 18));
        jToolBar1.add(currencyL);

        jMenu1.setText("Language");
        jMenu1.setToolTipText("");
        jMenu1.setFocusable(false);

        enUSMenuItem.setSelected(true);
        enUSMenuItem.setText("enUS");
=======
        dateL.setPreferredSize(new java.awt.Dimension(80, 18));
        jToolBar1.add(dateL);

        jMenu1.setText(Messages.getMessage("label.language.menu"));
        jMenu1.setToolTipText("");

        enUSMenuItem.setSelected(false);
        enUSMenuItem.setText(Messages.getMessage("label.language.en"));
>>>>>>> 467e6a535c00d72d5771a7d0a6c5434a045bfc30
        enUSMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                enUSMenuItemActionPerformed(evt);
            }
        });
        jMenu1.add(enUSMenuItem);

<<<<<<< HEAD
        ruRuMenuItem.setText("ruRu");
=======
        ruRuMenuItem.setText(Messages.getMessage("label.language.ru"));
        ruRuMenuItem.setSelected(true);
>>>>>>> 467e6a535c00d72d5771a7d0a6c5434a045bfc30
        ruRuMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ruRuMenuItemActionPerformed(evt);
            }
        });
        jMenu1.add(ruRuMenuItem);

        jMenuBar1.add(jMenu1);

        setJMenuBar(jMenuBar1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(resultL, javax.swing.GroupLayout.DEFAULT_SIZE, 236, Short.MAX_VALUE)
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(resultTextL)
                            .addComponent(functionTF, javax.swing.GroupLayout.PREFERRED_SIZE, 145, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(functionL))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(calculateB)))
                .addContainerGap(93, Short.MAX_VALUE))
            .addComponent(jToolBar1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(functionL)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(functionTF, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(calculateB))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(resultTextL)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(resultL, javax.swing.GroupLayout.PREFERRED_SIZE, 19, javax.swing.GroupLayout.PREFERRED_SIZE)
<<<<<<< HEAD
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
=======
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 25, Short.MAX_VALUE)
>>>>>>> 467e6a535c00d72d5771a7d0a6c5434a045bfc30
                .addComponent(jToolBar1, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void calculateBActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_calculateBActionPerformed
<<<<<<< HEAD
        String line = functionTF.getText();
        StringTokenizer st = new StringTokenizer(line, "(),");
        if (st.countTokens() > 1) {
            String func = st.nextToken();

            Number[] numbers = new Number[st.countTokens()];
            Class[] paramClass = new Class[st.countTokens()];
            for (int i = 0; i < numbers.length; ++i) {
                String param = st.nextToken();
                try {
                    numbers[i] = Integer.parseInt(param);
                    paramClass[i] = Integer.TYPE;
                } catch (NumberFormatException e) {
                    try {
                        numbers[i] = Double.parseDouble(param);
                        paramClass[i] = Double.TYPE;
                    } catch (NumberFormatException ex) {
                        resultL.setText(Messages.getMessage("error.wrong.args"));
                    }
                }
            }
            try {
                Method method = Math.class.getMethod(func, paramClass);
                Number result = (Number)method.invoke(Math.class, numbers);
                resultL.setText(String.valueOf(result));
            } catch (NoSuchMethodException ex) {
                resultL.setText(Messages.getMessage("error.no.method"));
            } catch (InvocationTargetException | IllegalAccessException e) {
                e.printStackTrace();
            }
        }
    }//GEN-LAST:event_calculateBActionPerformed

    private void enUSMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_enUSMenuItemActionPerformed
        enUSMenuItem.setSelected(true);
        ruRuMenuItem.setSelected(false);
        changeLocale(Messages.ENGLISH);
    }//GEN-LAST:event_enUSMenuItemActionPerformed

    private void ruRuMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ruRuMenuItemActionPerformed
        enUSMenuItem.setSelected(false);
        ruRuMenuItem.setSelected(true);
        changeLocale(Messages.RUSSIAN);
    }//GEN-LAST:event_ruRuMenuItemActionPerformed

    private void changeLocale(Locale locale) {
        Messages.setLocale(locale);
        setTitle(Messages.getMessage("title"));

        functionL.setText(Messages.getMessage("label.function") + " :");
        resultTextL.setText(Messages.getMessage("label.result") + " :");
        calculateB.setText(Messages.getMessage("label.calculate"));
        jMenu1.setText(Messages.getMessage("label.language.menu"));
        enUSMenuItem.setText(Messages.getMessage("label.language.en"));
        ruRuMenuItem.setText(Messages.getMessage("label.language.ru"));
        langL.setText(Messages.getMessage("language.show"));
        dateL.setText(DateFormat.getDateInstance(DateFormat.MONTH_FIELD, locale).format(curDate));
        currencyL.setText(NumberFormat.getCurrencyInstance(locale).format(1234567890.99));
        resultL.setText("");
    }

=======
        // TODO add your handling code here:
    }//GEN-LAST:event_calculateBActionPerformed

    private void enUSMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_enUSMenuItemActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_enUSMenuItemActionPerformed

    private void ruRuMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ruRuMenuItemActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_ruRuMenuItemActionPerformed

>>>>>>> 467e6a535c00d72d5771a7d0a6c5434a045bfc30
    public static void main(String args[]) {
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">

        try {
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException | UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(ClientFrame.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new ClientFrame().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton calculateB;
<<<<<<< HEAD
    private javax.swing.JLabel currencyL;
=======
>>>>>>> 467e6a535c00d72d5771a7d0a6c5434a045bfc30
    private javax.swing.JLabel dateL;
    private javax.swing.JCheckBoxMenuItem enUSMenuItem;
    private javax.swing.JLabel functionL;
    private javax.swing.JTextField functionTF;
    private javax.swing.JMenu jMenu1;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JToolBar.Separator jSeparator1;
<<<<<<< HEAD
    private javax.swing.JToolBar.Separator jSeparator2;
=======
>>>>>>> 467e6a535c00d72d5771a7d0a6c5434a045bfc30
    private javax.swing.JToolBar jToolBar1;
    private javax.swing.JLabel langL;
    private javax.swing.JLabel resultL;
    private javax.swing.JLabel resultTextL;
    private javax.swing.JCheckBoxMenuItem ruRuMenuItem;
    // End of variables declaration//GEN-END:variables
}

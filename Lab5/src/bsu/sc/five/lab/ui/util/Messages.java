package bsu.sc.five.lab.ui.util;

import java.util.Locale;
import java.util.ResourceBundle;

/**
 * @author vabramov
 */
public final class Messages {
    private static Messages instance = null;
    private ResourceBundle bundle = ResourceBundle.getBundle("messages", new Locale("ru")); // default
<<<<<<< HEAD
    public static final Locale RUSSIAN = new Locale.Builder().setLanguage("ru").setRegion("RU").build();
    public static final Locale ENGLISH = new Locale.Builder().setLanguage("en").setRegion("US").build();
=======
    public static final Locale RUSSIAN = new Locale("ru");
    public static final Locale ENGLISH = new Locale("en");
>>>>>>> 467e6a535c00d72d5771a7d0a6c5434a045bfc30
    private Messages() {

    }
    public static void setLocale(Locale locale) {
        init();
        instance.bundle = ResourceBundle.getBundle("messages", locale);
    }

    public static String getMessage(String key) {
        init();
        return instance.bundle.getString(key);
    }

    private static void init() {
        if (instance == null) {
            instance = new Messages();
        }
    }
}

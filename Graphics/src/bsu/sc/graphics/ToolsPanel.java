package bsu.sc.graphics;

import bsu.sc.graphics.entities.CustomColor;
import bsu.sc.graphics.tools.draw.*;
import bsu.sc.graphics.tools.modify.FloodTool;
import bsu.sc.graphics.tools.modify.StretchTool;
import bsu.sc.graphics.tools.modify.TransportTool;
import bsu.sc.graphics.utility.StretchDialog;
import bsu.sc.graphics.utility.TransportDialog;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * @author Alina Zabavskaya
 */
public class ToolsPanel extends JMenuBar {
    private static final long serialVersionUID = 5880755938635572586L;
    private static ToolsPanel instance = new ToolsPanel();
    private DrawTool tool;
    private LineTool lineTool = new LineTool();
    private CircleTool circleTool = new CircleTool();
    private AmputationTool amputationTool = new AmputationTool();
    private EllipseTool ellipseTool = new EllipseTool();
    private FloodTool floodTool = new FloodTool();
    private TransportTool transportTool = new TransportTool();
    private PolygonTool polygonTool = new PolygonTool();
    private StretchTool stretchTool = new StretchTool();
    private JMenuItem lineButton = new JMenuItem("Линия");
    private JMenuItem circleButton = new JMenuItem("Круг");
    private JMenuItem amputationButton = new JMenuItem("Вырезать");
    private JMenuItem ellipseButton = new JMenuItem("Эллисп");
    private JMenuItem floodButton = new JMenuItem("Заливка");
    private JMenuItem transportButton = new JMenuItem("Переместить");
    private JMenuItem polygonButton = new JMenuItem("Полигон");
    private JMenuItem stretchButton = new JMenuItem("Растяжение");
    private JMenuItem lineColorButton = new JMenuItem("Цвет линий");
    private JMenuItem floodColorButton = new JMenuItem("Звет заливки");
    private JMenu colorMenu = new JMenu("Цвет");
    private JMenu actionMenu = new JMenu("Действия");
    private CustomColor lineColor = new CustomColor(0, 0, 0);
    private CustomColor floodColor = new CustomColor(0, 0, 0);

    private ToolsPanel() {
        configureButtons();
        actionMenu.add(lineButton);
        actionMenu.add(circleButton);
        actionMenu.add(ellipseButton);
        actionMenu.add(polygonButton);
        actionMenu.add(floodButton);
        actionMenu.add(amputationButton);
        actionMenu.add(transportButton);
        actionMenu.add(stretchButton);
        colorMenu.add(lineColorButton);
        colorMenu.add(floodColorButton);
        add(actionMenu);
        add(colorMenu);
        tool = lineTool;
    }

    public static ToolsPanel getInstance() {
        return instance;
    }

    private void configureButtons() {
        lineButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                tool = lineTool;
            }
        });

        circleButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                tool = circleTool;
            }
        });

        amputationButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                tool = amputationTool;
            }
        });

        ellipseButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                tool = ellipseTool;
            }
        });
        polygonButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                tool = polygonTool;
            }
        });

        floodButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                floodTool.modify(0, 0);
                DrawPanel.getInstance().updateUI();
            }
        });

        transportButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                TransportDialog d = new TransportDialog(MyFrame.getInstance(), transportTool);
                d.setVisible(true);
                DrawPanel.getInstance().repaint();
            }
        });


        stretchButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                StretchDialog d = new StretchDialog(MyFrame.getInstance(), stretchTool);
                d.setVisible(true);
                DrawPanel.getInstance().updateUI();
            }
        });

        lineColorButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Color newColor = JColorChooser.showDialog(MyFrame.getInstance(),
                        "Выберите цвет",
                        new Color(lineColor.r, lineColor.g, lineColor.b));
                if (newColor != null) {
                    lineColor.r = newColor.getRed();
                    lineColor.g = newColor.getGreen();
                    lineColor.b = newColor.getBlue();
                }
            }
        });

        floodColorButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                Color newColor = JColorChooser.showDialog(MyFrame.getInstance(),
                        "Выберите цвет",
                        new Color(floodColor.r, floodColor.g, floodColor.b));
                if (newColor != null) {
                    floodColor.r = newColor.getRed();
                    floodColor.g = newColor.getGreen();
                    floodColor.b = newColor.getBlue();
                }
            }
        });
    }

    public DrawTool getTool() {
        return tool;
    }

    public CustomColor getLineColor() {
        return lineColor;
    }

    public CustomColor getFloodColor() {
        return floodColor;
    }
}

package bsu.sc.graphics;

import bsu.sc.graphics.entities.*;
import bsu.sc.graphics.entities.Point;
import bsu.sc.graphics.entities.figures.Figure;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.MouseWheelEvent;
import java.util.LinkedList;
import java.util.List;

/**
 * @author Alina Zabavskaya
 */
public class DrawPanel extends JPanel implements Scrollable {

    private static final long serialVersionUID = 2798166653515943602L;

    public List<Figure> getFigures() {
        return figures;
    }

    private class CanvasMouseAdapter extends MouseAdapter {
        @Override
        public void mousePressed(MouseEvent e) {
            int x = e.getX() / (zoom);
            int y = e.getY() / (zoom);
            if ((x < width) && (y < height)) {
                Figure f = ToolsPanel.getInstance().getTool().toolActivated(new Point(x, y));
                if(f!=null && !figures.contains(f))
                    figures.add(f);
                instance.repaint();
            }
        }

        @Override
        public void mouseWheelMoved(MouseWheelEvent e) {
            if (zoom > e.getWheelRotation())
                zoom -= e.getWheelRotation();
            else zoom = 1;
            setSize(width * zoom, height * zoom);
            setPreferredSize(new Dimension(width * zoom, height * zoom));
            instance.repaint();
        }
    }

    private static DrawPanel instance = null;
    private CustomColor bgColor = new CustomColor(255, 255, 255);
    private static DrawPanel secondInstance = null;
    private int zoom = 1;

    private int width = 800;
    private int height = 600;


    private List<Figure> figures = new LinkedList<>();


    public static DrawPanel getInstance() {
        if (instance == null)
            instance = new DrawPanel();
        return instance;
    }

    private DrawPanel() {
        this.setMinimumSize(new Dimension(800, 600));
        this.setPreferredSize(new Dimension(800, 600));
        this.setSize(800, 600);

        CanvasMouseAdapter mouseListener = new CanvasMouseAdapter();
        addMouseListener(mouseListener);
        addMouseMotionListener(mouseListener);
        addMouseWheelListener(mouseListener);
    }

    @Override
    public void paintComponent(Graphics g) {
        super.paintComponent(g);
        drawFigures(g);
    }

    public void drawFigures(Graphics g) {
        for (Figure f : figures)
            f.drawFigure(g, zoom);
    }

    @Override
    public Dimension getPreferredScrollableViewportSize() {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public int getScrollableUnitIncrement(Rectangle visibleRect, int orientation, int direction) {
        return 0;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public int getScrollableBlockIncrement(Rectangle visibleRect, int orientation, int direction) {
        return 0;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public boolean getScrollableTracksViewportWidth() {
        return false;  //To change body of implemented methods use File | Settings | File Templates.
    }

    @Override
    public boolean getScrollableTracksViewportHeight() {
        return false;  //To change body of implemented methods use File | Settings | File Templates.
    }
}

package bsu.sc.graphics.utility;

import bsu.sc.graphics.entities.Point;
import bsu.sc.graphics.entities.figures.Figure;

import java.util.HashSet;
import java.util.Stack;

/**
 * @author Alina Zabavskaya
 */
public class Flood {
    public static HashSet<Point> floodFigure(Figure f) {

        Point drawPoint = f.getInnerPixel();
        if (drawPoint == null)
            return new HashSet<>();
        HashSet<Point> flood = new HashSet<>();
        Stack<Point> stack = new Stack<>();
        stack.push(drawPoint);
        while (!stack.isEmpty()) {
            Point p = stack.pop();
            flood.add(p);
            nextDrawPixels(p, f, stack, flood);
        }
        return flood;
    }

    private static void nextDrawPixels(Point p, Figure f, Stack<Point> stack, HashSet<Point> flood) {
        Point neighbour = new Point(p.x + 1, p.y);

        if (!f.getPoints().contains(neighbour) && !flood.contains(neighbour))
            stack.push((Point) neighbour.clone());

        neighbour.y++;
        neighbour.x--;

        if (!f.getPoints().contains(neighbour) && !flood.contains(neighbour))
            stack.push((Point) neighbour.clone());

        neighbour.x--;
        neighbour.y--;

        if (!f.getPoints().contains(neighbour) && !flood.contains(neighbour))
            stack.push((Point) neighbour.clone());

        neighbour.y--;
        neighbour.x++;

        if (!f.getPoints().contains(neighbour) && !flood.contains(neighbour))
            stack.push((Point) neighbour.clone());
    }
}

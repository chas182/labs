package bsu.sc.graphics.utility;

import bsu.sc.graphics.tools.modify.TransportTool;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * @author Alina Zabavskaya
 */
public class TransportDialog extends JDialog {
    TransportTool tool;

    public TransportDialog(Frame owner, final TransportTool tool) {
        super(owner, true);
        setMinimumSize(new Dimension(200, 100));
        setLayout(new FlowLayout());
        this.tool = tool;
        JButton ok = new JButton("ok");
        final JTextField x = new JTextField(4), y = new JTextField(4);
        x.setText("0");
        y.setText("0");
        add(new JLabel("x: "));
        add(x);
        add(new JLabel("y: "));
        add(y);
        add(ok);
        ok.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                tool.modify(Integer.parseInt(x.getText()), Integer.parseInt(y.getText()));
                setVisible(false);
            }
        });
    }
}

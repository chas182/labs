package bsu.sc.graphics;

import javax.swing.*;

/**
 * @author Alina Zabavskaya
 */
public class Invoker {
    public static void main(String[] args) {
        try {
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException | UnsupportedLookAndFeelException e) {
            e.printStackTrace();
        }

        JFrame frame = MyFrame.getInstance();
        frame.setVisible(true);
    }
}

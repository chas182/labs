package bsu.sc.graphics.entities.figures;

import bsu.sc.graphics.entities.CustomColor;
import bsu.sc.graphics.entities.Point;

import java.awt.*;
import java.util.Collection;
import java.util.Set;

/**
 * @author Alina Zabavskaya
 */
public class Raster extends Figure {
    public Raster() {
        super(new CustomColor(0, 0, 0));
    }

    public void setPixel(Point p, CustomColor c) {
        addPixel(new ColoredPoint(c, p.x, p.y));
    }

    public void setPixels(Collection<Point> ps, CustomColor c) {
        for (Point p : ps)
            setPixel(p, c);
    }

    @Override
    public void drawFigure(Graphics g, int zoom) {
        Color prev = g.getColor();
        for (Point point : getPoints()) {
            CustomColor color = ((ColoredPoint) point).c;
            g.setColor(new Color(color.r, color.g, color.b));
            g.fillRect(point.x * zoom, point.y * zoom, zoom, zoom);
        }
        g.setColor(prev);
    }

    @Override
    public Set<Point> toPixels() {
        notModified();
        return getPoints();
    }

    @Override
    public void move(int x, int y) {
        // not supported
    }

    @Override
    public void stretch(double x, double y) {
        // not supported
    }

    @Override
    public Set<Point> getFlood() {
        return getPoints();
    }

    @Override
    public Point getInnerPixel() {
        return null;
    }

    private class ColoredPoint extends Point {
        CustomColor c;

        public ColoredPoint(CustomColor c, int x, int y) {
            super(x, y);
            this.c = c;
        }
    }
}

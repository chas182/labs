package bsu.sc.graphics.entities.figures;

import bsu.sc.graphics.entities.CustomColor;
import bsu.sc.graphics.entities.Point;
import bsu.sc.graphics.utility.Flood;

import java.util.HashSet;
import java.util.Set;

/**
 * @author Alina Zabavskaya
 */
public class Ellipse extends Figure {
    public int a;
    public int b;
    public Point center;

    public Ellipse(CustomColor c, Point cen, int a, int b) {
        super(c);
        center = cen;
        this.a = a;
        this.b = b;
    }

    @Override
    public Set<Point> getFlood() {
        return Flood.floodFigure(this);
    }

    @Override
    public Point getInnerPixel() {
        return center;
    }

    @Override
    public Set<Point> toPixels() {
        return ellipse();
    }

    @Override
    public void move(int x, int y) {
        center.x += x;
        center.y += y;
        modify();
    }

    @Override
    public void stretch(double x, double y) {
        a = (int) (a * x);
        b = (int) (b * x);
        modify();
    }

    public HashSet<Point> ellipse() {
        HashSet<Point> points = new HashSet<>();
        int xCenter = center.x;
        int yCenter = center.y;
        for (int x = 1; x <= a; x++) {
            int xp = x - 1;
            int y = (int) Math.sqrt(b * b - b * b * x * x / (a * a));
            int yp = (int) Math.sqrt(b * b - b * b * xp * xp / (a * a));
            points.addAll(Line.newLinePixels(this.getColor(), new Point(xCenter + xp, yCenter + yp), new Point(xCenter + x, yCenter + y)));
            points.addAll(Line.newLinePixels(this.getColor(), new Point(xCenter - xp, yCenter + yp), new Point(xCenter - x, yCenter + y)));
            points.addAll(Line.newLinePixels(this.getColor(), new Point(xCenter + xp, yCenter - yp), new Point(xCenter + x, yCenter - y)));
            points.addAll(Line.newLinePixels(this.getColor(), new Point(xCenter - xp, yCenter - yp), new Point(xCenter - x, yCenter - y)));
        }
        return points;
    }
}

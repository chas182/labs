package bsu.sc.graphics.entities.figures;

import bsu.sc.graphics.entities.CustomColor;
import bsu.sc.graphics.entities.Point;
import bsu.sc.graphics.utility.Flood;

import java.util.*;

/**
 * @author Alina Zabavskaya
 */
public class Polygon extends Figure {
    public List<Point> points = new LinkedList<>();
    private boolean finished = false;

    public Polygon(CustomColor c, List<Point> pts) {
        super(c);
        points = pts;
    }

    public void addPoint(Point p) {
        if (!finished) {
            if (points.size() > 2) {
                Set<Point> poly = polygon();
                Set<Point> newLine = Line.newLinePixels(getColor(), points.get(points.size() - 1), p);
                newLine.retainAll(poly);
                Set<Point> firstLine = Line.newLinePixels(getColor(), points.get(0), points.get(1));
                if (!newLine.isEmpty())
                    for (Point pix : newLine)
                        if (firstLine.contains(pix)) {
                            points.add(pix);
                            finished = true;
                            modify();
                            return;
                        }
                points.add(p);
            }
            points.add(p);
        }
        modify();
    }

    public boolean getFinished() {
        return finished;
    }

    @Override
    public Set<Point> toPixels() {
        return polygon();
    }

    public HashSet<Point> polygon() {
        HashSet<Point> pix = new HashSet<>();
        if (!points.isEmpty()) {
            Point prev = null;
            for (Point point : points) {
                if (prev != null)
                    pix.addAll(Line.newLinePixels(this.getColor(), prev, point));
                prev = point;
            }
            if (finished)
                pix.addAll(Line.newLinePixels(this.getColor(), prev, points.get(0)));
        }
        return pix;
    }

    @Override
    public void move(int x, int y) {
        for (Point p : points) {
            p.x += x;
            p.y += y;
        }
        modify();
    }

    @Override
    public void stretch(double x, double y) {
        Point min = getPoint();
        for (Point point : points) {
            point.x = min.x + (int) (x * (point.x - min.x));
            point.y = min.y + (int) (y * (point.y - min.y));
        }
        modify();
    }

    @Override
    public Set<Point> getFlood() {
        if (finished)
            return Flood.floodFigure(this);
        else return new HashSet<>();
    }

    @Override
    public Point getInnerPixel() {
        if (!finished || points.size() < 3)
            return null;
        int x = 0;
        int y = 0;
        for (Point p : points) {
            x += p.x;
            y += p.y;
        }
        x /= points.size();
        y /= points.size();
        return new Point(x, y);
    }

    @Override
    public void flood(CustomColor c) {
        if (finished)
            super.flood(c);
    }

    private Point getPoint() {
        int x = Collections.min(getPoints(), new Comparator<Point>() {
            @Override
            public int compare(Point o1, Point o2) {
                return o1.x - o2.x;
            }
        }).x;

        int y = Collections.min(getPoints(), new Comparator<Point>() {
            @Override
            public int compare(Point o1, Point o2) {
                return o1.y - o2.y;
            }
        }).y;
        return new Point(x, y);
    }
}

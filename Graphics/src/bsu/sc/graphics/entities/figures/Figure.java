package bsu.sc.graphics.entities.figures;

import bsu.sc.graphics.entities.*;
import bsu.sc.graphics.entities.Point;

import java.awt.*;
import java.util.HashSet;
import java.util.Set;

/**
 * @author Alina Zabavskaya
 */
public abstract class Figure {
    private Set<Point> points = new HashSet<>();
//    private Set<Point> floodPoints = new HashSet<>();
    private CustomColor color;
    private boolean modified = true;
    private CustomColor floodColor;


    public Figure(CustomColor c) {
        color = c;
    }

    public void addPixel(Point p) {
        points.add(p);
    }

    public void drawFigure(Graphics g, int zoom) {
        Color prev = g.getColor();
        g.setColor(new Color(color.r, color.g, color.b));
        for (Point point : getPoints()) {

            g.fillRect(point.x * zoom, point.y * zoom, zoom, zoom);
        }
        if (floodColor != null) {
            g.setColor(new Color(floodColor.r, floodColor.g, floodColor.b));
            for (Point point : getFlood()) {
                g.fillRect(point.x * zoom, point.y * zoom, zoom, zoom);
            }
        }
        g.setColor(prev);
    }

    public Set<Point> getPoints() {
        if (modified) {
            points = toPixels();
            modified = false;
        }
        return points;
    }

    protected void notModified() {
        modified = false;
    }

    public CustomColor getColor() {
        return color;
    }

    public void flood(CustomColor c) {
        floodColor = c;
        modify();
    }

    public void modify() {
        modified = true;
    }

    public CustomColor getFloodColor() {
        return floodColor;
    }

    public abstract Set<Point> toPixels();

    public abstract void move(int x, int y);

    public abstract void stretch(double x, double y);

    public abstract Set<Point> getFlood();

    public abstract bsu.sc.graphics.entities.Point getInnerPixel();
}

package bsu.sc.graphics.entities.figures;

import bsu.sc.graphics.entities.CustomColor;
import bsu.sc.graphics.entities.Point;

import java.util.HashSet;
import java.util.Set;

/**
 * @author Alina Zabavskaya
 */
public class Line extends Figure {
    public Point start;
    public Point end;

    public Line(CustomColor color, Point s, Point e) {
        super(color);
        start = s;
        end = e;
    }

    public static Set<Point> newLinePixels(CustomColor c, Point s, Point e) {
        return new Line(c, s, e).toPixels();
    }

    @Override
    public Set<Point> getFlood() {
        return new HashSet<>();
    }

    @Override
    public Point getInnerPixel() {
        return null;
    }

    @Override
    public Set<Point> toPixels() {
        return drawBrez();
    }

    @Override
    public void move(int x, int y) {
        start.x += x;
        end.x += x;
        start.y += y;
        end.y += y;
        modify();
    }

    @Override
    public void stretch(double x, double y) {
        end.x = start.x + (int) ((end.x - start.x) * x);
        end.y = start.y + (int) ((end.y - start.y) * y);
        modify();
    }

    private HashSet<Point> drawBrez() {
        HashSet<Point> points = new HashSet<>();
        int x, y, dx, dy, incx, incy, pdx, pdy, es, el, err;

        dx = end.x - start.x;
        dy = end.y - start.y;

        incx = sign(dx);
        incy = sign(dy);

        if (dx < 0) dx = -dx;
        if (dy < 0) dy = -dy;


        if (dx > dy) {
            pdx = incx;
            pdy = 0;
            es = dy;
            el = dx;
        } else {
            pdx = 0;
            pdy = incy;
            es = dx;
            el = dy;
        }

        x = (int) start.x;
        y = (int) start.y;
        err = el / 2;

        points.add(new Point(x, y));

        for (int t = 0; t < el; t++) {
            err -= es;
            if (err < 0) {
                err += el;
                x += incx;
                y += incy;
            } else {
                x += pdx;
                y += pdy;
            }
            points.add(new Point(x, y));
        }

        return points;
    }

    private int sign(int x) {
        return (x > 0) ? 1 : (x < 0) ? -1 : 0;
    }
}

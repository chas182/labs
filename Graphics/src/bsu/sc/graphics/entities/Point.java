package bsu.sc.graphics.entities;

/**
 * @author Alina Zabavskaya
 */
public class Point implements Cloneable {
    public int x, y;
    private boolean visible = true;

    public Point(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public boolean isVisible() {
        return visible;
    }

    public void setVisible(boolean v) {
        visible = v;
    }

    @Override
    public Object clone() {
        return new Point(x, y);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (!(o instanceof Point)) return false;

        Point point = (Point) o;

        if (x != point.x) return false;
        if (y != point.y) return false;

        return true;
    }

    @Override
    public int hashCode() {
        int result = x;
        result = 31 * result + y;
        result = 31 * result + (visible ? 1 : 0);
        return result;
    }

    @Override
    public String toString() {
        return "(x=" + x + "; y=" + y + ")";
    }
}

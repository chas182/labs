package bsu.sc.graphics.entities;

/**
 * @author Alina Zabavskaya
 */
public class CustomColor {
    public int r,g,b;

    public CustomColor(int rr, int gg, int bb)
    {
        r=rr;
        g=gg;
        b=bb;
    }
}

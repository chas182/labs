package bsu.sc.graphics.tools.modify;

import bsu.sc.graphics.entities.figures.Figure;

import java.util.List;

/**
 * @author Alina Zabavskaya
 */
public class StretchTool extends ModifyTool {

    @Override
    protected void executeModification(List<Figure> figs, double x, double y) {
        Figure f = figs.get(figs.size()-1);
        f.stretch(x,y);
    }
}

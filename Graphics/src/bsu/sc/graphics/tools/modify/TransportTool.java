package bsu.sc.graphics.tools.modify;

import bsu.sc.graphics.entities.figures.Figure;

import java.util.List;

/**
 * @author Alina Zabavskaya
 */
public class TransportTool extends ModifyTool {

    @Override
    protected void executeModification(List<Figure> figs, double x, double y) {
        Figure f = figs.get(figs.size()-1);
        f.move((int)x,(int)y);
    }
}

package bsu.sc.graphics.tools.modify;

import bsu.sc.graphics.DrawPanel;
import bsu.sc.graphics.entities.figures.Figure;

import java.util.List;

/**
 * @author Alina Zabavskaya
 */
public abstract class ModifyTool {
    public void modify(double x, double y) {
        List<Figure> figs = DrawPanel.getInstance().getFigures();
        if (figs == null || figs.isEmpty())
            return;
        executeModification(figs, x, y);
    }

    protected abstract void executeModification(List<Figure> figs, double x, double y);
}

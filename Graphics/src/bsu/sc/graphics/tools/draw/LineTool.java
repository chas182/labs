package bsu.sc.graphics.tools.draw;

import bsu.sc.graphics.ToolsPanel;
import bsu.sc.graphics.entities.Point;
import bsu.sc.graphics.entities.figures.Figure;
import bsu.sc.graphics.entities.figures.Line;

public class LineTool implements DrawTool {
    Point firstCoord = null;
    Point secondCoord;

    @Override
    public Figure toolActivated(Point coordinates) {
        if (firstCoord == null) {
            firstCoord = coordinates;
            return null;
        } else {
            secondCoord = coordinates;
            Line line = new Line(ToolsPanel.getInstance().getLineColor(),firstCoord,secondCoord);
            firstCoord = null;
            return line;
        }
    }
}

package bsu.sc.graphics.tools.draw;

import bsu.sc.graphics.DrawPanel;
import bsu.sc.graphics.ToolsPanel;
import bsu.sc.graphics.entities.Point;
import bsu.sc.graphics.entities.figures.Figure;
import bsu.sc.graphics.entities.figures.Polygon;

import java.util.LinkedList;
import java.util.List;

/**
 * @author Alina Zabavskaya
 */
public class PolygonTool implements DrawTool {

    @Override
    public Figure toolActivated(Point Coordinates) {
        Polygon polygon;
        List<Figure> figs = DrawPanel.getInstance().getFigures();
        if (!figs.isEmpty())
            if (figs.get(figs.size() - 1) instanceof Polygon) {
                Polygon prev = (Polygon) figs.get(figs.size() - 1);
                if (!prev.getFinished()) {
                    polygon = prev;
                    polygon.addPoint(Coordinates);
                    return polygon;
                }
            }
        polygon = new Polygon(ToolsPanel.getInstance().getLineColor(), new LinkedList<Point>());
        polygon.addPoint(Coordinates);
        figs.add(polygon);
        return polygon;
        //maybe need to add to figures
    }
}

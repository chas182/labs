package bsu.sc.graphics.tools.draw;

import bsu.sc.graphics.entities.Point;
import bsu.sc.graphics.entities.figures.Figure;

/**
 * @author Alina Zabavskaya
 */
public interface DrawTool {
    public Figure toolActivated(Point Coordinates);
}

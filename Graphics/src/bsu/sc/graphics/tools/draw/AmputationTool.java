package bsu.sc.graphics.tools.draw;

import bsu.sc.graphics.DrawPanel;
import bsu.sc.graphics.entities.Point;
import bsu.sc.graphics.entities.figures.Figure;
import bsu.sc.graphics.entities.figures.Raster;

import java.util.Iterator;
import java.util.List;

/**
 * @author Alina Zabavskaya
 */
public class AmputationTool implements DrawTool {
    Point firstCoord = null;
    Point secondCoord;

    @Override
    public Figure toolActivated(Point coordinates) {
        if (firstCoord == null) {
            firstCoord = coordinates;
            return null;
        } else {
            secondCoord = coordinates;
            Raster r = amputate(firstCoord, secondCoord);
            firstCoord = null;
            return r;
        }
    }

    public Raster amputate(Point d1, Point d2) {
        List<Figure> figures = DrawPanel.getInstance().getFigures();
        Raster r = new Raster();
        if (d2.x < d1.x) {
            Point d = d1;
            d1 = d2;
            d2 = d;
        }
        for (Figure f : figures) {
            for (Iterator<Point> i = f.getPoints().iterator(); i.hasNext(); ) {
                Point d = i.next();
                if (d1.x <= d.x && d.x <= d2.x &&
                    d1.y <= d.y && d.y <= d2.y) {
                    r.setPixel(d, f.getColor());
                }
            }
            for (Iterator<Point> i = f.getFlood().iterator(); i.hasNext(); ) {
                Point d = i.next();
                if (d1.x <= d.x && d.x <= d2.x &&
                    d1.y <= d.y && d.y <= d2.y) {
                    if (f.getFloodColor() != null)
                        r.setPixel(d, f.getFloodColor());
                }
            }
        }
        figures.clear();
        figures.add(r);
        return r;
    }
}

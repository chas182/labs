package bsu.sc.graphics.tools.draw;

import bsu.sc.graphics.ToolsPanel;
import bsu.sc.graphics.entities.Point;
import bsu.sc.graphics.entities.figures.Ellipse;
import bsu.sc.graphics.entities.figures.Figure;

/**
 * @author Alina Zabavskaya
 */
public class CircleTool implements DrawTool {
    Point firstCoord = null;
    Point secondCoord;

    @Override
    public Figure toolActivated(Point coordinates) {
        if (firstCoord == null) {
            firstCoord = coordinates;
            return null;
        } else {
            secondCoord = coordinates;
            int radius = (int) Math.sqrt(Math.pow(firstCoord.y - secondCoord.y, 2)
                    + Math.pow(firstCoord.x - secondCoord.x, 2));
            Ellipse e = new Ellipse(ToolsPanel.getInstance().getLineColor(),firstCoord,radius,radius);
            firstCoord = null;
            return e;
        }
    }
}

package bsu.sc.graphics.tools.draw;

import bsu.sc.graphics.ToolsPanel;
import bsu.sc.graphics.entities.Point;
import bsu.sc.graphics.entities.figures.Ellipse;
import bsu.sc.graphics.entities.figures.Figure;

/**
 * @author Alina Zabavskaya
 */
public class EllipseTool implements DrawTool {
    Point firstCoord = null;
    Point secondCoord;

    @Override
    public Figure toolActivated(Point coordinates) {
        if (firstCoord == null) {
            firstCoord = coordinates;
            return null;
        } else {
            secondCoord = coordinates;
            int xCenter = (firstCoord.x + secondCoord.x) / 2;
            int yCenter = (firstCoord.y + secondCoord.y) / 2;
            Ellipse ellipse = new Ellipse(ToolsPanel.getInstance().getLineColor(),
                    new Point(xCenter,yCenter),
                    Math.abs(firstCoord.x - xCenter),Math.abs(firstCoord.y - yCenter));
            firstCoord = null;
            return ellipse;
        }
    }
}

package bsu.sc.graphics;

import javax.swing.*;
import java.awt.*;

/**
 * @author Alina Zabavskaya
 */
public class MyFrame extends JFrame {

    private static final long serialVersionUID = 9086193599418056563L;
    private static MyFrame instance = null;
    ToolsPanel tools = ToolsPanel.getInstance();
    DrawPanel drawPanel = DrawPanel.getInstance();

    private MyFrame() {
        super("Лабораторная");
        initialize();
        appendToolPanel();
        appendCanvas();
    }

    public static MyFrame getInstance() {
        if (instance == null)
            instance = new MyFrame();
        return instance;
    }

    public void setDrawPanel(DrawPanel panel) {
        drawPanel = panel;
        repaint();
    }

    private void initialize() {
        setDefaultCloseOperation(EXIT_ON_CLOSE);
        setLayout(new GridBagLayout());
        setSize();
        setLayout(new BorderLayout());
    }

    private void setSize() {
        setMinimumSize(new Dimension(800, 600));
        setPreferredSize(new Dimension(800, 600));
    }

    private void appendToolPanel() {

        setJMenuBar(tools);
    }

    private void appendCanvas() {
        JScrollPane scrollCanvas = new JScrollPane(drawPanel);
        add(scrollCanvas, BorderLayout.CENTER);
    }
}

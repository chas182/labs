package bsu.sc.third.lab.client;

import java.io.Serializable;

/**
 * @author vlad
 */
public class SenderInfo implements Serializable, Cloneable {
    private static final long serialVersionUID = 6743257440544786822L;
    private String recipientName;
    private String senderName;
    private String fileName;

    public SenderInfo() {

    }

    public SenderInfo(String senderName) {
        this.senderName = senderName;
    }

    public String getRecipientName() {
        return recipientName;
    }

    public void setRecipientName(String recipientName) {
        this.recipientName = recipientName;
    }

    public String getSenderName() {
        return senderName;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public SenderInfo clone() {
        SenderInfo newObject = new SenderInfo();
        newObject.senderName = this.senderName;
        newObject.fileName = this.fileName;
        return newObject;
    }

    public void clean() {
        this.recipientName = "";
        this.fileName = "";
    }
}

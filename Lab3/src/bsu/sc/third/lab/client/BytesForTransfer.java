package bsu.sc.third.lab.client;

import java.io.Serializable;

/**
 * @author vlad
 */
public class BytesForTransfer implements Serializable {
    private static final long serialVersionUID = -2159705321345359734L;
    private byte[] data;

    public byte[] getData() {
        return data;
    }

    public void setData(byte[] data) {
        this.data = data;
    }
}

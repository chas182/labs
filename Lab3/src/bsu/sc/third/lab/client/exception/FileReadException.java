package bsu.sc.third.lab.client.exception;

/**
 * @author vlad
 */
public class FileReadException extends RuntimeException {
    private static final long serialVersionUID = -1971518134056843968L;

    /**
     * Constructs a new runtime exception with {@code null} as its
     * detail message.  The cause is not initialized, and may subsequently be
     * initialized by a call to {@link #initCause}.
     */
    public FileReadException() {
        super();
    }

    /**
     * Constructs a new runtime exception with the specified detail message.
     * The cause is not initialized, and may subsequently be initialized by a
     * call to {@link #initCause}.
     *
     * @param message the detail message. The detail message is saved for
     *                later retrieval by the {@link #getMessage()} method.
     */
    public FileReadException(String message) {
        super(message);
    }

}

package bsu.sc.third.lab.client;

import bsu.sc.third.lab.client.exception.FileReadException;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 * @author Vlad
 */
public final class TransferManager {

    private static final int FIVE_MEGA_BYTES = 5242880;
    private File file = null;
    private boolean isLoaded = false;
    private transient byte[] fileData;

    public TransferManager() {
    }

    public boolean setFile(File file) {
        this.file = file;       //5 080 953
        if (this.file.length() <= FIVE_MEGA_BYTES) {
            this.isLoaded = true;
        }
        return isLoaded;
    }

    public boolean isFileLoaded() {
        return isLoaded;
    }

    public String getFileName() {
        return file.getName();
    }

    public byte[] getEncodedBytesFromFile(String encodeStr) {
        readFile();

        encode(encodeStr);

        return fileData;
    }

    public void saveDecodedFile(File selectedFile, byte[] data, String encodeStr) {
        try (FileOutputStream fos = new FileOutputStream(selectedFile)) {
            fileData = data;

            encode(encodeStr);
            fos.write(data);
        } catch (IOException ex) {
            System.err.println(ex.getMessage());
        }
    }

    private void encode(String encode) {
        int k = 0;

        for (int i = 0; i < fileData.length; ++i) {
            fileData[i] = (byte) (fileData[i] ^ (byte) encode.charAt(k++));
            if (k == encode.length()) {
                k = 0;
            }
        }
    }

    private void readFile() throws FileReadException {
        try (FileInputStream fis = new FileInputStream(file)) {
            fileData = new byte[(int) file.length()];
            int resultLength = fis.read(fileData);
            if (resultLength != file.length()) {
                throw new FileReadException("File is not read");
            }
        } catch (IOException ex) {
            System.err.println(ex.getMessage());
        }
    }

    public void clean() {
        this.file = null;
        this.fileData = null;
        this.isLoaded = false;
    }
}

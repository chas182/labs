package bsu.sc.third.lab.client;

import bsu.sc.third.lab.client.exception.FileReadException;
import bsu.sc.third.lab.server.Server;

import javax.swing.*;
import java.io.*;
import java.net.Socket;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author Vlad
 */
public class Client extends JFrame {

    private static final long serialVersionUID = -7536220313042024017L;
    private JFileChooser fc = new JFileChooser();
    private TransferManager transferManager = new TransferManager();
    private SenderInfo senderInfo;
    private ObjectOutputStream out;
    private ObjectInputStream in;
    private JLabel fileName;
    private JLabel jLabel1;
    private JButton openButton;
    private JTextField recipient;
    private JButton sendButton;
    private String itsName;

    /**
     * Creates new form Client
     */
    public Client() throws IOException {
        initComponents();
        connectToServer();
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String args[]) throws IOException {
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        try {
            UIManager.setLookAndFeel(UIManager.getSystemLookAndFeelClassName());
        } catch (ClassNotFoundException | InstantiationException | IllegalAccessException | UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Client.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>

        Client client = new Client();
        client.setVisible(true);
        client.run();
    }

    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        openButton = new JButton();
        recipient = new JTextField();
        jLabel1 = new JLabel();
        sendButton = new JButton();
        fileName = new JLabel();

        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);

        openButton.setText("Открыть файл");
        openButton.setEnabled(false);
        openButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                openFile();
            }
        });

        recipient.setEnabled(false);

        jLabel1.setText("Получатель:");

        sendButton.setText("Отправить");
        sendButton.setEnabled(false);
        sendButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                sendFile();
            }
        });

        GroupLayout layout = new GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
                layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                        .addGroup(layout.createSequentialGroup()
                                .addContainerGap()
                                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                                        .addGroup(layout.createSequentialGroup()
                                                .addComponent(openButton)
                                                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                                                .addComponent(fileName, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                                        .addGroup(layout.createSequentialGroup()
                                                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                                                        .addComponent(jLabel1)
                                                        .addComponent(recipient, GroupLayout.PREFERRED_SIZE, 171, GroupLayout.PREFERRED_SIZE))
                                                .addGap(0, 98, Short.MAX_VALUE))
                                        .addGroup(layout.createSequentialGroup()
                                                .addComponent(sendButton)

                                                .addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))))
        );
        layout.setVerticalGroup(
                layout.createParallelGroup(GroupLayout.Alignment.LEADING)
                        .addGroup(layout.createSequentialGroup()
                                .addContainerGap()
                                .addComponent(jLabel1)
                                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(recipient, GroupLayout.PREFERRED_SIZE, GroupLayout.DEFAULT_SIZE, GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING, false)
                                        .addComponent(fileName, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        .addComponent(openButton, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                                .addPreferredGap(LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(layout.createParallelGroup(GroupLayout.Alignment.LEADING, false)
                                        .addComponent(sendButton, GroupLayout.DEFAULT_SIZE, GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                                .addContainerGap(GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void sendFile() {//GEN-FIRST:event_sendButtonActionPerformed
        if (transferManager.isFileLoaded()) {
            try {
                String recipientName = recipient.getText();
                if (recipientName.length() > 0) {
                    senderInfo.setRecipientName(recipientName);

                    out.writeObject(senderInfo);
                    out.flush();
                    senderInfo = senderInfo.clone();
                } else {
                    JOptionPane.showMessageDialog(this, "Введите имя пользователя");
                }
            } catch (Exception ex) {
                Logger.getLogger(Client.class.getName()).log(Level.SEVERE, null, ex);
            }
        } else {
            JOptionPane.showMessageDialog(this, "Откройте файл");
        }
    }//GEN-LAST:event_sendButtonActionPerformed

    private void openFile() {//GEN-FIRST:event_openButtonActionPerformed
        if (JFileChooser.APPROVE_OPTION == fc.showOpenDialog(this)) {

            if (!transferManager.setFile(fc.getSelectedFile())) {
                JOptionPane.showMessageDialog(this, "Файл большой, выберите другой");
                return;
            }

            fileName.setText(transferManager.getFileName());
            senderInfo.setFileName(transferManager.getFileName());
        }
    }//GEN-LAST:event_openButtonActionPerformed

    private void connectToServer() throws IOException {
        Socket socket = new Socket("127.0.0.1", Server.PORT);
        out = new ObjectOutputStream(socket.getOutputStream());
        in = new ObjectInputStream(socket.getInputStream());
    }

    public void run() throws IOException {
        TransferManager forIncomingData = new TransferManager();
        SenderInfo inputSenderInfo = new SenderInfo();
        while (true) {
            try {
                Object input = in.readObject();
                if (input instanceof String) {
                    String line = (String) input;
                    if (line.startsWith("GiveNameForRegistration")) {
                        getClientName(true);
                    } else if (line.startsWith("ThisNameAlreadyExists")) {
                        getClientName(false);
                    } else if (line.startsWith("RegistrationSuccessful")) {
                        openButton.setEnabled(true);
                        sendButton.setEnabled(true);
                        recipient.setEnabled(true);

                        senderInfo = new SenderInfo(itsName);
                        setTitle("Имя: " + itsName);

                        System.out.println("Its name -> " + itsName);
                    } else if (line.startsWith("RecipientDoesNotExists")) {
                        JOptionPane.showMessageDialog(this, "Такого пользователя не существует.");
                    } else if (line.startsWith("TransferError")) {
                        JOptionPane.showMessageDialog(this, "Во время передачи возникла ошибка");
                    } else if (line.startsWith("RecipientExists")) {
                        fileName.setText("");
                        String encode = getEncodeString();
                        while ("".equals(encode) || encode == null) {
                            encode = getEncodeString();
                        }

                        BytesForTransfer bytes = new BytesForTransfer();
                        try {
                            bytes.setData(transferManager.getEncodedBytesFromFile(encode));
                        } catch (FileReadException ex) {
                            JOptionPane.showMessageDialog(this, "Ошибка при чтении файла.");
                            out.writeObject("FileNotRead");
                            out.flush();
                            return;
                        }
                        out.writeObject(bytes);
                        out.flush();
                        transferManager.clean();
                    }
                } else if (input instanceof SenderInfo) {
                    inputSenderInfo = (SenderInfo) input;
                } else if (input instanceof BytesForTransfer) {
                    JOptionPane.showMessageDialog(this, "Пользователь \"" + inputSenderInfo.getSenderName() + "\" прислал Вам файл.");
                    String encode = getEncodeString();
                    while ("".equals(encode) || encode == null) {
                        encode = getEncodeString();
                    }
                    BytesForTransfer bytes = (BytesForTransfer) input;
                    fc.setSelectedFile(new File(Server.DEFAULT_DOWNLOAD_DIR + inputSenderInfo.getFileName()));
                    if (JFileChooser.APPROVE_OPTION == fc.showSaveDialog(this)) {
                        System.out.println("Save file: " + fc.getSelectedFile().getName());

                        forIncomingData.saveDecodedFile(fc.getSelectedFile(), bytes.getData(), encode);

                    }

                }
            } catch (ClassNotFoundException ex) {
                System.err.println(ex.getMessage());
            } catch (EOFException ex) {
                JOptionPane.showMessageDialog(this, "Сервер отключен, приложение будет закрыто.");
                System.exit(0);
            }
        }
    }

    private void cleanData() {
        senderInfo.clean();
    }

    private String getEncodeString() {
        return JOptionPane.showInputDialog(
                this,
                "Введите строчку для кодирования:",
                "Кодовая строка",
                JOptionPane.PLAIN_MESSAGE);
    }

    /**
     * @param flag {@code true} if no error, {@code false} if name is exists.
     */
    private void getClientName(boolean flag) throws IOException {
        String name;

        while (true) {
            name = nameDialog(flag);
            if (name != null && !"".equals(name)) {
                break;
            }
        }
        itsName = name;
        out.writeObject(name);
        out.flush();
    }

    private String nameDialog(boolean flag) {
        return JOptionPane.showInputDialog(
                this,
                (flag) ? "Введите свое имя:" : "Такое имя уже существует.\nВведите другое:",
                "Ввод имени",
                JOptionPane.PLAIN_MESSAGE);
    }
}

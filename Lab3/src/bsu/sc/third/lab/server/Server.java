package bsu.sc.third.lab.server;

import bsu.sc.third.lab.client.BytesForTransfer;
import bsu.sc.third.lab.client.SenderInfo;
import bsu.sc.third.lab.client.TransferManager;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.BindException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.HashMap;
import java.util.HashSet;

/**
 * @author Vlad
 */
public class Server {

    public static final String DEFAULT_DOWNLOAD_DIR = "/home/vlad/Documents/Programming/Labs/lab3download/";
    /**
     * The port that the server listens on.
     */
    public static final int PORT = 9090;
    /**
     * The set of all names of clients for the transfer. Maintained so that we
     * can check that new clients are not registering name already in use.
     */
    private static HashSet<String> names = new HashSet<>();
    /**
     * The set of all the print writers for all the clients. This set is kept so
     * we can easily broadcast messages.
     */
    private static HashMap<String, ObjectOutputStream> writers = new HashMap<>();

    public static void main(String[] args) throws IOException {
        System.out.println("Server is running.");

        try (ServerSocket listener = new ServerSocket(PORT)) {
            while (true) {
                new Handler(listener.accept()).start();
            }
        } catch (BindException e) {
            System.err.print("\n" + e.getMessage() + "\n");
            System.exit(1);
        }
    }

    private static class Handler extends Thread {

        private String name;
        private Socket socket;
        private ObjectInputStream in;
        private ObjectOutputStream out;

        public Handler(Socket accept) {
            this.socket = accept;
        }

        @Override
        public void run() {
            try {
                in = new ObjectInputStream(socket.getInputStream());
                out = new ObjectOutputStream(socket.getOutputStream());
                ObjectOutputStream recipientOut = null;
                SenderInfo senderInfo;
                BytesForTransfer bytesForTransfer;

                if (getClientName()) {
                    return;
                }

                while (true) {
                    Object input = in.readObject();
                    if (input == null) {
                        return;
                    } else if (input instanceof String) {
                        String message = (String) input;
                        if (message.startsWith("FileNotRead")) {
                            recipientOut.writeObject("TransferError");
                            recipientOut.flush();
                        }

                    } else if (input instanceof BytesForTransfer) {
                        bytesForTransfer = (BytesForTransfer) input;

                        recipientOut.writeObject(bytesForTransfer);
                        recipientOut.flush();
                    } else if (input instanceof SenderInfo) {
                        senderInfo = (SenderInfo) input;

                        if (checkName(senderInfo.getRecipientName())) {

                            System.out.println("Sender - " + senderInfo.getSenderName() + "  |  Recipient - " + senderInfo.getRecipientName());
                            recipientOut = writers.get(senderInfo.getRecipientName());
                            recipientOut.writeObject(senderInfo);

                            out.writeObject("RecipientExists");
                            out.flush();
                        } else {
                            out.writeObject("RecipientDoesNotExists");
                            out.flush();
                        }
                    }
                }
            } catch (IOException | ClassNotFoundException ex) {
                System.err.println(ex.getMessage());
            } finally {
                // This client is going down!  Remove its name and its print
                // writer from the sets, and close its socket.
                if (name != null) {
                    System.out.println("User with name: " + name + "  disconnected from the server.");
                    writers.remove(name);
                    names.remove(name);
                }
                try {
                    socket.close();
                } catch (IOException e) {
                    System.err.println(e.getMessage());
                }
            }
        }

        // <editor-fold defaultstate="collapsed" desc="Getting user name">
        private boolean getClientName() throws ClassNotFoundException, IOException {
            out.writeObject("GiveNameForRegistration");
            out.flush();
            while (true) {
                Object temp = in.readObject();
                if (temp == null) {
                    return true;
                } else {
                    if (temp.getClass().equals(String.class)) {
                        name = (String) temp;
                    } else {
                        return true;
                    }
                }
                synchronized (names) {
                    if (!names.contains(name)) {
                        names.add(name);
                        break;
                    } else {
                        out.writeObject("ThisNameAlreadyExists");
                    }
                }
            }
            out.writeObject("RegistrationSuccessful");
            writers.put(name, out);
            System.out.println("User with name: " + name + "  connected to the server.");
            return false;
        }
        // </editor-fold>

        private synchronized boolean checkName(String name) throws IOException {
            if (names.contains(name)) {
                System.out.println(true);
//                out.writeObject("RECIPIENTEXISTS");
                return true;
            } else {
                System.out.println(false);
//                out.writeObject("RECIPIENTDOESNOTEXISTS");
                return false;
            }
        }
    }
}

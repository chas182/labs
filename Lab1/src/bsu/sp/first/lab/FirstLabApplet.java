package bsu.sp.first.lab;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

/**
 * @author Vlad
 */
public class FirstLabApplet extends JApplet {
    private static final String PARAM_NAME = "fieldString";
    private static final String PARAM_FONT_NAME = "fontName";
    private static final String PARAM_FONT_SIZE = "fontSize";
    private static final String PARAM_FONT_COLOR = "fontColor";
    private static final String DEFAULT_STRING = "Enter";
    private static final String DEFAULT_FONT_NAME = "Times New Roman";
    private static final int DEFAULT_FONT_SIZE = 11;
    private JTextField funcField;

    public void init() {
        try {
            javax.swing.SwingUtilities.invokeAndWait(new Runnable() {
                public void run() {
                    createGUI();
                }
            });
        } catch (Exception e) {
            System.err.println("createGUI didn't successfully complete");
        }
    }

    private void createGUI() {
        final String param = getParameter(PARAM_NAME);
        getContentPane().setLayout(new BorderLayout());

        funcField = new JTextField((param != null ? param : DEFAULT_STRING));

        String fontName = getParameter(PARAM_FONT_NAME);
        if (fontName == null) fontName = DEFAULT_FONT_NAME;

        int fontSize;
        try {
            fontSize = Integer.parseInt(getParameter(PARAM_FONT_SIZE));
        } catch (NumberFormatException e) {
            fontSize = DEFAULT_FONT_SIZE;
        }

        String color = getParameter(PARAM_FONT_COLOR);
        Color fontColor = (color == null) ? Color.black : Color.decode(color);

        funcField.setFont(new Font(fontName, Font.PLAIN, fontSize));

        funcField.setForeground(fontColor);

        funcField.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                if (funcField.getText().equals(param)) {
                    funcField.setText("");
                    return;
                }
                try {
                    Double.parseDouble(funcField.getText());
                } catch (NumberFormatException e1) {
                    return;
                }
                funcField.setText("");
            }
        });

        funcField.addKeyListener(new KeyAdapter() {
            @Override
            public void keyTyped(KeyEvent e) {
                if (e.getKeyChar() == '\n') {
                    if (TaskUtil.checkBrackets(getField())) {
                        try {
                            funcField.setText(TaskUtil.calculate(getField()) + "");
                        } catch (IllegalArgumentException e1) {
                            JOptionPane.showMessageDialog(null, "String is invalid");
                        }
                    } else {
                        JOptionPane.showMessageDialog(null, "Missing '(' or ')'");
                    }
                }
            }
        });

        JButton helpButton = new JButton("Help");

        helpButton.addActionListener(new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                JOptionPane.showMessageDialog(null, "Use sin, cos, tg and ctg. And operations: +, -\nFor example:\ntg(35)   cos(60)+sin(30)");
            }
        });

        JButton enterButton = new JButton("Calculate");

        enterButton.addActionListener(new AbstractAction() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (TaskUtil.checkBrackets(getField())) {
                    try {
                        funcField.setText(TaskUtil.calculate(getField()) + "");
                    } catch (IllegalArgumentException e1) {
                        JOptionPane.showMessageDialog(null, "String is invalid");
                    }
                } else {
                    JOptionPane.showMessageDialog(null, "Missing '(' or ')'");
                }
            }
        });

        getContentPane().add(funcField, BorderLayout.CENTER);

        JPanel buttonPanel = new JPanel(new BorderLayout());

        buttonPanel.add(enterButton, BorderLayout.WEST);
        buttonPanel.add(helpButton, BorderLayout.EAST);

        getContentPane().add(buttonPanel, BorderLayout.SOUTH);
    }

    private String getField() {
        return funcField.getText();
    }
}

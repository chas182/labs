package bsu.sp.first.lab;

/**
 * @author Vlad
 */
public enum FunctionType {
    SIN,
    COS,
    TG,
    CTG;
}

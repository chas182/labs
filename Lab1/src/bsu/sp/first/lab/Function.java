package bsu.sp.first.lab;

/**
 * @author Vlad
 */
public class Function {
    private FunctionType typeOfFunc;
    private double arg;

    public void setTypeOfFunc(String typeOfFunc) {
        this.typeOfFunc = FunctionType.valueOf(typeOfFunc.toUpperCase());
    }

    public void setArg(String arg) throws NumberFormatException {
//        this.arg = Math.toDegrees(Double.parseDouble(arg));
        this.arg = Math.toRadians(Double.parseDouble(arg));
    }

    public double calculate() {
        double result = 0.0;
        switch (typeOfFunc) {
            case COS:
                result = Math.cos(arg);
                break;
            case SIN:
                result = Math.sin(arg);
                break;
            case TG:
                result = Math.tan(arg);
                break;
            case CTG:
                result = 1 / Math.tan(arg);
                break;
        }
        return result;
    }
}

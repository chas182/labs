package bsu.sp.first.lab;

import java.util.ArrayList;

/**
 * @author Vlad
 */
public final class TaskUtil {
    private static double result;
    private static ArrayList<Character> operations = new ArrayList<Character>();

    private TaskUtil() {
    }

    public static boolean checkBrackets(String input) {
        int k = 0;
        for (char ch : input.toCharArray()) {
            if (ch == '(') k++;
            if (ch == ')') k--;
        }
        return (k == 0);
    }

    public static double calculate(String input) throws IllegalArgumentException {
        initOperations(input);

        calculateResult(input);
        operations.clear();

        return result;
    }

    private static void calculateResult(String input) throws IllegalArgumentException {
        int numberOfOperation = 0, lastIndex = 0;
        double prev, curResult = 0.0;

        for (int i = 0; i < input.length(); ++i) {
            char ch = input.charAt(i);

            if (isOperation(ch) || i == input.length() - 1) {
                String func = input.substring(
                        lastIndex,
                        (i == input.length() - 1 ? i + 1 : i)
                );
                lastIndex = input.indexOf(ch) + 1;
                prev = curResult;
                curResult = calculateFunction(func);
                switch (operations.get(numberOfOperation++)) {
                    case '+':
                        curResult = prev + curResult;
                        break;
                    case '-':
                        curResult = prev - curResult;
                        break;
                }
            }

        }
        result = curResult;
    }

    private static double calculateFunction(String function) {
        String type = function.substring(0, function.indexOf('('));
        Function curCalculateFunction = new Function();
        curCalculateFunction.setTypeOfFunc(type);
        curCalculateFunction.setArg(
                function.substring(
                        function.indexOf('(') + 1, function.indexOf(')')
                )
        );
        return curCalculateFunction.calculate();
    }

    private static boolean isOperation(char ch) {
        return ch == '+' || ch == '-';// || ch == '*' || ch == '/';
    }

    private static void initOperations(String str) {
        operations.add('+');
        for (char ch : str.toCharArray()) {
            if (isOperation(ch)) {
                operations.add(ch);
            }
        }
    }
}

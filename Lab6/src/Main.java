import java.io.*;

/**
 * @author Vlad
 */
public class Main {
    public static void main(String[] args) {
        if (args.length == 1) {
            String file = args[0];
            String javaFile = "";
            try (FileInputStream fis = new FileInputStream(file)) {
                File file1 = new File(file);
                byte[] bytes = new byte[(int)file1.length()];
                int n = fis.read(bytes);
                if (n == -1 || n < file1.length()) {
                    return;
                }
                javaFile = new String(bytes);
            } catch (FileNotFoundException e) {
              System.out.println("File not found");
            } catch (IOException e) {
              System.out.println(e.getMessage());
            }
            String newFile = javaFile.replaceAll("(?:/\\*(?:[^*]|(?:\\*+[^*/]))*\\*+/)|(?://.*)","");
//            String newFile = javaFile.replaceAll("/\\*(?:.|[\\n\\r])*?\\*/","");
//            String newFile = javaFile.replaceAll("(/\\*.*\\*/)|(//.*(?=\\\\n))","");
//            String newFile = javaFile.replaceAll("//.*|(\"(?:\\\\[^\"]|\\\\\"|.)*?\")|(?s)/\\*.*?\\*/\"","");
            try (FileOutputStream fos = new FileOutputStream(file)) {
                fos.write(newFile.getBytes());
            }catch (IOException e) {
                System.out.println(e.getMessage());
            }
        } else {
            System.out.println("Invalid args");
        }
    }
}
